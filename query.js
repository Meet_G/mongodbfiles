let arr = []
db.skills_taxonomy.aggregate([{
    "$match": {
      "is_deleted": false
    }
  },
  {
    "$project": {
      "skill_name": 1,
      "_id": 1
    }
  }
]).forEach(obj => {
  arr.push(obj.skill_name)
})
let updateArr = []
db.skills_taxonomy.aggregate([{
    "$project": {
      "synonyms": 1,
      "skill_id": 1,
      "_id": 0
    }
  },
  {
    "$unwind": "$synonyms"
  },
  {
    "$match": {
      "synonyms": {
        "$in": arr
      }
    }
  }
]).forEach(obj => {
  let updateData =
    db.skills_taxonomy.update({
      "skill_id": obj.skill_id
    }, {
      $pull: {
        synonyms: {
          $in: [obj.synonyms]
        }
      }
    }, {
      multi: true
    })
  updateArr.push(updateData)
})
print(updateArr)
