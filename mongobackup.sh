#!/bin/bash

NOW=`date +"%y-%m-%d_%H.%M"`
OUT_DIR=/mnt/appdata/backups/db/$NOW
mkdir -p $OUT_DIR
cd $OUT_DIR

echo "Backup directory = $OUT_DIR"

MONGO_HOSTS="production/prodswarm3:27017,prodswarm5:27017"

# echo "MongoDB Hosts = $MONGO_HOSTS"

# Try to fetch list of databases
#DB_LIST=$(mongo --host "$MONGO_HOSTS" --quiet --eval  "printjson(db.adminCommand('listDatabases'))" | grep -vE '^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3}[+-][0-9]{4}\s+')
#echo "DB_LIST = $DB_LIST"

do_backup()
{
  DB_NAME=$1
  BACKUP_FILE=$2
  OUT_DIR=$3

  BACKUP_FILE="$DB_NAME"_"$NOW".archive
  echo Backing up  $DB_NAME to $BACKUP_FILE
  mongodump -u 'scikey' -p 'aYvI05WOB362cRgD3vSt9JqKES8V7sf5' --host "$MONGO_HOSTS" --authenticationDatabase 'admin' --db $DB_NAME --gzip --quiet --archive=$BACKUP_FILE
  #cp $BACKUP_FILE $OUT_DIR
  #rm $BACKUP_FILE

}

#$(echo $DB_LIST | jq '.databases | map(.name) | .[]' | tr -d '"')

ARR=(hire_scikey futurxlabs_scikey demo_scikey demofin_scikey demohire_scikey app_serenaway enrich_serenaway futurxlabs_sereaway futurxlabs_steerhigh srkaycg_steerhigh app_hirest career_srk demo_hirest)

for key in ${ARR[@]}
do

  DB_NAME=$key

  # Skip backing up local database
  if [ "$key" = "local" ]
  then
    # echo Skipping local database
    continue
  fi

  do_backup "$DB_NAME" "$BACKUP_FILE" "$OUT_DIR"

done
